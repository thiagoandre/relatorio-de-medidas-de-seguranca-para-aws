# Relatório de Medidas de Segurança para AWS

Com a crescente adoção de serviços em nuvem, como a AWS (Amazon Web Services), a segurança da informação torna-se uma preocupação essencial para empresas de todos os tamanhos. Nesse contexto, é fundamental implementar medidas de segurança adequadas para proteger os dados e sistemas hospedados na nuvem. Este relatório apresenta três medidas de segurança que podem ser utilizadas em conjunto com a AWS para tornar uma empresa mais segura.


## 1. Controle de Acesso IAM (Identity and Access Management):

O IAM da AWS oferece um conjunto robusto de ferramentas para gerenciar identidades e acessos aos recursos da nuvem. Por meio do IAM, é possível criar e gerenciar usuários, grupos e permissões de forma granular, garantindo que apenas pessoas autorizadas tenham acesso aos recursos necessários. Recomenda-se a implementação de políticas de least privilege, ou seja, conceder o mínimo de privilégios necessários para cada usuário ou sistema. Além disso, o uso de MFA (Multi-Factor Authentication) adiciona uma camada adicional de segurança, exigindo não apenas a senha, mas também um segundo fator de autenticação, como um token ou SMS, para acessar a conta. Essas práticas ajudam a mitigar os riscos de acessos não autorizados e a proteger os dados da empresa.


## 2. Monitoramento de Logs e Análise de Segurança:

- __CloudTrail:__ O AWS CloudTrail registra todas as ações realizadas na conta da AWS, fornecendo um registro completo de quem fez o que e quando. Isso ajuda na detecção de atividades suspeitas e na investigação de incidentes. Ao analisar os registros do CloudTrail, é possível identificar padrões de acesso incomuns ou tentativas de comprometimento da conta, permitindo uma resposta rápida a possíveis ameaças.

- __Amazon GuardDuty:__ O GuardDuty é um serviço de detecção de ameaças que monitora continuamente as atividades da AWS em busca de comportamentos maliciosos. Utilizando análise de machine learning e correlação de eventos, o GuardDuty pode identificar atividades como tentativas de acesso não autorizado, comunicações com IPs maliciosos e padrões de tráfego anômalos. Ao detectar uma ameaça, o GuardDuty gera alertas para que a equipe de segurança possa tomar medidas corretivas imediatas. Essas ferramentas são essenciais para manter a segurança da conta da AWS e proteger os dados da empresa contra ameaças cibernéticas.


## 3. Proteção de Dados em Repouso e em Trânsito:

- __Amazon S3 Encryption:__ O Amazon S3 oferece opções de criptografia para proteger os dados armazenados. É possível utilizar a criptografia SSE (Server-Side Encryption) ou SSE-KMS (Server-Side Encryption with AWS Key Management Service) para criptografar os dados automaticamente antes de serem armazenados no S3. Isso garante que mesmo se os dados forem acessados por terceiros não autorizados, eles permaneçam ilegíveis e protegidos.

- __Amazon Virtual Private Cloud (VPC):__ O Amazon VPC permite criar uma rede virtual isolada na AWS, na qual é possível controlar o tráfego de entrada e saída. Utilizando o VPC, é possível definir regras de segurança, como listas de controle de acesso (ACLs) e grupos de segurança, para restringir o acesso aos recursos da nuvem. Além disso, é possível utilizar VPN (Virtual Private Network) para criar uma conexão segura entre a infraestrutura local e a nuvem da AWS, garantindo que os dados em trânsito sejam protegidos contra interceptação por terceiros não autorizados. Essas medidas garantem a proteção dos dados em repouso e em trânsito, garantindo a integridade e confidencialidade das informações da empresa.


Em conclusão, a segurança da informação é um aspecto crucial para empresas que utilizam serviços em nuvem, como a AWS. As medidas apresentadas neste relatório, incluindo o controle de acesso IAM, o monitoramento de logs e análise de segurança com o CloudTrail e o GuardDuty, e a proteção de dados em repouso e em trânsito com o Amazon S3 Encryption e o Amazon VPC, são essenciais para garantir a proteção dos dados e sistemas da empresa. É fundamental que as empresas implementem essas medidas de segurança em conjunto com as práticas recomendadas pela AWS e mantenham-se atualizadas sobre as novas ameaças e vulnerabilidades. Assim, será possível manter um ambiente seguro na nuvem e proteger os ativos mais importantes da empresa.
